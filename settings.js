export const components = {
  BATT: {
    frequency: 3,
    type: 'under',
    target: {
      name: 'RED LOW',
      index: 5,
    },
    interval_mins: 5,
  },
  TSTAT: {
    frequency: 3,
    type: 'over',
    target: {
      name: 'RED HIGH',
      index: 2,
    },
    interval_mins: 5,
  },
  // for extended tests
  ENRG: {
    frequency: 5,
    type: 'over',
    target: {
      name: 'YELLOW HIGH',
      index: 3,
    },
    interval_mins: 10,
  },
};
