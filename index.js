import fs from 'fs';
import dotenv from 'dotenv';
import SatReading from './js-plugins/sat-reading.js';
import trackAlerts from './js-plugins/track-alerts.js';
import report from './js-plugins/report.js';

dotenv.config();

const { SAT_READINGS_FILEPATH } = process.env;

const satData = fs
  .readFileSync(SAT_READINGS_FILEPATH, {
    encoding: 'ascii',
  })
  .split('\n');

// wrap with helper class, filter to only flagged items for efficiency, sort by time
const satReadings = satData
  .map((reading) => new SatReading(reading))
  .filter((satReading) => satReading.isFlagged)
  .sort((a, b) => a.timestamp.getTime() - b.timestamp.getTime());

report(trackAlerts(satReadings));
