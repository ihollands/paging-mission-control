export default (satReadings) => {
  // track unique satellites and target alerts
  const satellites = {},
    alerts = [];

  satReadings.forEach((satReading) => {
    const { id, timestamp, intervalMs, componentName, alertMessage } =
      satReading;

    // allow for any number of satellites, instantiating on first encounter
    if (!satellites[id]) {
      satellites[id] = {};
    }

    // allow for any number of telemetric components, instantiating alert on first encounter
    if (!satellites[id][componentName]) {
      satellites[id][componentName] = {
        count: 1,
        time: timestamp.getTime(),
        message: alertMessage,
      };
      return;
    }

    if (timestamp.getTime() - intervalMs < satellites[id][componentName].time) {
      // within interval, increment count for satellite/component
      satellites[id][componentName].count += 1;
    } else {
      // outside of interval, delete component to reset count and time
      delete satellites[id][componentName];
      return;
    }

    if (satellites[id][componentName].count === 3) {
      alerts.push(satellites[id][componentName].message);
      delete satellites[id][componentName];
    }
  });

  return alerts;
};
