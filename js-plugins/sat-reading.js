import { components } from '../settings.js';

export default class SatReading {
  constructor(data) {
    // private props
    this._rawData = data.split('|');
    const [_time, _id, , , , , _rawValue, _componentName] = this._rawData;
    this._rawValue = _rawValue;
    this._alertSettings = components[_componentName];

    // public props
    this.timestamp = this._convertTimestamp(_time);
    this.id = _id;
    this.componentName = _componentName;
    this.intervalMs = this._alertSettings.interval_mins * 60000;
    this.isFlagged = this._getIsFlagged();
    this.alertMessage = {
      satelliteId: parseInt(_id, 10),
      severity: this._alertSettings.target.name,
      component: _componentName,
      timestamp: this.timestamp.toISOString(),
    };
  }

  // private methods
  _getIsFlagged() {
    const {
        type,
        target: { index },
      } = this._alertSettings,
      alertThreshold = parseFloat(this._rawData[index]),
      rawValue = parseFloat(this._rawValue);

    if (type === 'over') {
      return rawValue > alertThreshold;
    }

    if (type === 'under') {
      return rawValue < alertThreshold;
    }
  }

  _convertTimestamp(ts) {
    const [tsDate, tsTime] = ts.split(' '),
      date = new Date(`${getDate(tsDate)}T${tsTime}Z`);

    return date;

    function getDate(dateString) {
      const year = tsDate.substring(0, 4),
        month = tsDate.substring(4, 6),
        day = tsDate.substring(6);

      return [year, month, day].join('-');
    }
  }
}
